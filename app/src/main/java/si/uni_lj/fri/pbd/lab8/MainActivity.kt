package si.uni_lj.fri.pbd.lab8

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import si.uni_lj.fri.pbd.lab8.databinding.MainActivityBinding
import si.uni_lj.fri.pbd.lab8.ui.main.MainFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(binding.container.id, MainFragment.newInstance())
                    .commitNow()
        }
    }
}