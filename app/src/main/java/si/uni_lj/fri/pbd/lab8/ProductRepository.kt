package si.uni_lj.fri.pbd.lab8

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


class ProductRepository(application: Application?) {
    val searchResults = MutableLiveData<List<Product>>()
    val allProducts: LiveData<List<Product>>


    // TODO: Add DAO reference
    private val productDao: ProductDao

    fun insertProduct(newproduct: Product) {
        // TODO: run query to insert a product on the executor
        ProductRoomDatabase.databaseWriteExecutor.execute(Runnable {
            productDao.insertProduct(newproduct)
        })
    }

    fun deleteProduct(name: String) {
        // TODO: run query to delete a product on the executor
        ProductRoomDatabase.databaseWriteExecutor.execute(Runnable {
            productDao.deleteProduct(name)
        })
    }

    fun findProduct(name: String) {
        // TODO: run query on the executor, postValue to searchResults
        ProductRoomDatabase.databaseWriteExecutor.execute(Runnable {
            searchResults.postValue(productDao.findProduct(name))
        })
    }


    init {
        // TODO: Add the code in the init block
        val db: ProductRoomDatabase =
                ProductRoomDatabase.getDatabase(application?.applicationContext!!)!!
        productDao = db.productDao()
        allProducts = productDao.allProducts
    }
}