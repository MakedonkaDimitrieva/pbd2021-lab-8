package si.uni_lj.fri.pbd.lab8

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products")
class Product {

    @ColumnInfo(name = "productId")
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "productName")
    var name: String? = null

    @ColumnInfo(name = "productQuantity")
    var quantity = 0
}