package si.uni_lj.fri.pbd.lab8.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.lab8.Product
import si.uni_lj.fri.pbd.lab8.ProductRepository

class MainViewModel(application: Application?) : AndroidViewModel(application!!) {
    // TODO: add LiveData fields (allProducts and searchResults)
    var allProducts: LiveData<List<Product>>
    var searchResults: MutableLiveData<List<Product>>

    // TODO: add ProductRepository field
    private val repository: ProductRepository

    // TODO: add functions for inserting, deleting, and finding a product
    fun insertProduct(product: Product) {
        repository.insertProduct(product)
    }
    fun findProduct(name: String) {
        repository.findProduct(name)
    }
    fun deleteProduct(name: String) {
        repository.deleteProduct(name)
    }

    init {

        // TODO: set repository, allProducts, and searchResults
        repository = ProductRepository(application)
        allProducts = repository.allProducts
        searchResults = repository.searchResults

    }
}