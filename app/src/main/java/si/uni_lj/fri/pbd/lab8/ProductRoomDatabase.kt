package si.uni_lj.fri.pbd.lab8

import android.content.Context
import androidx.room.Database
import androidx.room.Room.databaseBuilder
import androidx.room.RoomDatabase
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized
import java.util.concurrent.Executors

@Database(entities = [Product::class], version = 1)
abstract class ProductRoomDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    companion object {
        private var INSTANCE: ProductRoomDatabase? = null
        private const val NUMBER_OF_THREADS = 4
        val databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS)

        @OptIn(InternalCoroutinesApi::class)
        fun getDatabase(context: Context) : ProductRoomDatabase? {
            if(INSTANCE == null) {
                synchronized(ProductRoomDatabase::class.java) {
                    if(INSTANCE == null) {
                        INSTANCE = databaseBuilder(
                                context.applicationContext,
                                ProductRoomDatabase::class.java,
                                "product_database").build()
                    }
                }
            }
            return INSTANCE
        }
    }
}